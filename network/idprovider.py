

class IDProvider():

    def __init__(self, size):

        # range from 0 to size-1
        self.size = size
        self.__free_nums = dict.fromkeys(range(0, size), 0)
        self.__taken_nums = {}

    def get_status(self):
        """

        :return: a tuple (number of free spots, number of taken spots)
        """
        return len(self.__free_nums), len(self.__taken_nums)

    def get(self):
        # Check if spaces left
        if len(self.__free_nums) == 0:
            return None

        num_pair = self.__free_nums.popitem()
        self.__taken_nums[num_pair[0]] = 0
        return num_pair[0]

    def release(self, num):

        if not self.__taken_nums:
            raise Exception("Attempted to release when no IDs where in use!")

        num_pair = (num, 0) if self.__taken_nums.pop(num, None) == 0 else None

        if num_pair == None:
            raise Exception("Attempted to release {} unused id, number was in free_nums: {}".format(num, num in self.__free_nums))

        self.__free_nums[num_pair[0]] = 0
