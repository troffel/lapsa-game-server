
class Cmd:

    TYPE_MOVE = "10"
    TYPE_UI_STATE = "11"
    TYPE_HEART_BEAT = "12"
    TYPE_HANDSHAKE = "13"
    TYPE_HANDSHAKE_RESPONSE = "14"
    TYPE_IS_PLAYABLE = "15"

    TYPE_INTERNAL_KICK = "1"

    @staticmethod
    def to_move(x, y):
        """
        Formats movement as: x direction + 2 digit x move + y direction + 2 digit y move.
        Direction formatted as 2 for positive and 1 for negative.
        2,-3 = 102003
        :param x:
        :param y:
        :return:
        """
        # get direction
        x_dir = 1 if x < 0 else 2
        y_dir = 1 if y < 0 else 2
        # get the absolute values for the concatenation
        x, y = abs(x), abs(y)
        # format the movement values with two digits
        x = x if x > 9 else "0{}".format(x)
        y = y if y > 9 else "0{}".format(y)
        return "{}{}{}{}".format(x_dir, x, y_dir, y)

    @staticmethod
    def from_move(move):
        """

        :param move: string
        :return: Reverse of to_move(x,y), returning a tuple
        """

        x_dir = int(move[:1])
        x = int(move[1:3])
        y_dir = int(move[3:4])
        y = int(move[4:6])

        if x_dir < 2:
            x *= -1

        if y_dir < 2:
            y *= -1

        return x, y

    @staticmethod
    def to_players(*players):
        """

        convert a player to a string representation, containing the following data:

        position, name,

        :param player: string representation of player
        :return: string representation to send to client.
        """
        cmd_players = []
        for player in players:
            cmd_players.append("{}:{}:{}".format(player.x, player.y, player.name))
        return ','.join(map(str, cmd_players))

    @staticmethod
    def to_ui_state(*cmd_strings):
        return "{}-{}".format(Cmd.TYPE_UI_STATE, '.'.join(cmd_strings))

    @staticmethod
    def to_hand_shake():
        return "{}".format(Cmd.TYPE_HANDSHAKE)

    @staticmethod
    def from_hand_shake(handshake):
        pass

    @staticmethod
    def to_heart_beat():
        return "{}".format(Cmd.TYPE_HEART_BEAT)

    @staticmethod
    def from_heart_beat(heartbeat):
        pass


class Movement:

    @staticmethod
    def is_movement(action):
        """

        :param action: string 2 letters followed by 6 integers, all in one long string
        :return: true if a movement command
        """
        return action[:2] == Cmd.TYPE_MOVE
