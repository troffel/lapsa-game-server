import queue
import select
import selectors
import socket
import threading

from network import cmd
from network import idprovider


class Adapter:
    class Listener:
        def __init__(self, on_player_join, on_player_leave, on_player_interaction):
            """

            :param on_player_join: takes an adapter.Connection as parameter, ONLY accept if player.authenticated()
            :param on_player_leave: takes an adapter.Connection ass parameter
            :param on_player_interaction: takes a connection and a stringformat command
            :return: void
            """
            self.on_player_join = on_player_join
            self.on_player_leave = on_player_leave # TODO: Handle this gracefully
            self.on_player_action = on_player_interaction

    def __init__(self, host, port, size, listener):

        self.port = port
        self.host = host

        self.connections = []

        self.poller = None

        self.selector = selectors.DefaultSelector()

        self.idprovider = idprovider.IDProvider(size)

        self.exit_flag = False

        self.event_listener = listener

    def start(self):

        self.exit_flag = False

        # start polling connected and listen for new connections
        self.poller = threading.Thread(target=self.poll)
        self.poller.daemon = True
        self.poller.start()

    def stop(self):
        self.exit_flag = True

    def accept(self, socket, mask):
        print("SERVER: accepted client")
        clientsocket, addr = socket.accept()

        id = self.idprovider.get()
        connection = Connection(clientsocket, id)

        if id is not None:
            clientsocket.setblocking(False)
            self.connections.append(connection)
            connection.handshake(False)
        else:
            # TODO: Inform that the server was full
            print("SERVER: Refused connection as no more ids are available")
            # clientsocket.send("Server full!")
            connection.handshake(True)
            clientsocket.close()

    def read(self, conn, mask):
        data = conn.socket.recv(1000)  # Should be ready
        print("SERVER: " + data)

        if data:
            clean_data = data.decode().rstrip()
            #TODO: HANDLE PEOPLE BLINDLY SPAMMING, CHECK THAT PLAYER IS IN CONNECTED STATE
            print('SERVER: received', repr(clean_data), 'from', conn)
            if clean_data == cmd.Cmd.TYPE_HANDSHAKE:
                # handshake always send by server, not client!
                self.disconnect_user(conn)
            elif clean_data == cmd.Cmd.TYPE_HANDSHAKE_RESPONSE:
            # client responded to handshake,
                print("SERVER: GOT HANDSHAKE RESPONSE")
                self.event_listener.on_player_join(conn)
            else:
                self.event_listener.on_player_action(conn, clean_data)  # strip newline from messasge
        else:
            self.disconnect_user(conn)

    def poll(self):
        """
        ! Runnable !
        Poll all connected sockets. On event, add pending event to all subscribers of player, on stuff to write, write!
        :return:
        """
        # create a socket object
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # serversocket = socket.socket()

        serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        serversocket.setblocking(False)
        serversocket.bind((self.host, self.port))
        serversocket.listen(100)

        self.connections.append(serversocket)

        try:
            while not self.exit_flag:
                import time
                time.sleep(.5)
                #print("polling: " + str(not self.exit_flag))

                readable, writeable, errors = select.select(self.connections, self.connections, self.connections)
                for s in readable:
                    if s == serversocket:
                        self.accept(s, None)
                    else:
                        # https: // stackoverflow.com / q / 28129518
                        # except ConnectionResetError: data = 0
                        # might want to wrap the read in a try catch for the above error, seen others do!
                        self.read(s, None)

                for s in writeable:
                    while not s.queued_messages.empty():
                        msg = s.queued_messages.get()
                        if msg == cmd.Cmd.TYPE_INTERNAL_KICK:
                            self.disconnect_user(s)
                        else:
                            try:
                                s.socket.send("{}{}".format(msg, "\n").encode())
                            except Exception as e:
                                print("{} occoured while sending {} to {}".format(e, msg, s.session_id))



                for s in errors:
                    print("Errors:")
                    data = s.socket.recv(1024)
                    print("data: ", data)
                    self.disconnect_user(s)

            print("Network Adapter stopped")

        finally:
            # clean up
            serversocket.close()

    def disconnect_user(self, connection):
        print("SERVER: disconnecting", connection)
        self.event_listener.on_player_leave(connection) # callback? mark conn dirty)
        self.connections.remove(connection)
        # TODO: Send DC message
        connection.disconnect()


class Connection:

    def __init__(self, socket, id):
        self.queued_messages = queue.Queue()

        self.socket = socket
        self.subscriptions = []
        self.session_id = id

        self.ready_to_receive_UI_state = False

        # Reference to player, and player has a reference to connection
        self.player = None

    def has_writable(self):
        return not self.queued_messages.empty()

    def is_in_game(self):
        return self.player and self.player.connection.ready_to_receive_UI_state

    def send(self, msg):
        print("SERVER: sending " + msg)
        self.queued_messages.put(msg)

    def fileno(self):
        # wrapper https://docs.python.org/3/library/select.html#select.select
        return self.socket.fileno()

    def disconnect(self):
        self.socket.close()

    def handshake(self, full):
        self.send(cmd.Cmd.to_hand_shake())

    def heartbeat(self):
        self.send(cmd.Cmd.to_heart_beat())

    def on_ready(self):
        if self.player is None:
            raise Exception("Connection signaled ready without having a player object")
        self.send(cmd.Cmd.TYPE_IS_PLAYABLE)
        self.player.connection.ready_to_receive_UI_state = True

    def on_removed(self):
        pass

    def rejected(self):
        # Kick the player off since an illegal action occured
        self.send(cmd.Cmd.TYPE_INTERNAL_KICK)
