from unittest import TestCase
from models.map import LeafLinker
from models.map import Tree


class TestLeafLinker(TestCase):
    def test_map_adjacent_nodes(self):
        size = 2 ** 5
        min_size = 4
        world = Tree(0, 0, size, None, min_size)
        LeafLinker(world).map_adjacent_nodes()

        top_left_leaf = world.get_leaf_node(0, 0)

        size = top_left_leaf.size

        self.assertTrue(top_left_leaf.right is world.get_leaf_node(size, 0))
        self.assertTrue(top_left_leaf.bottom is world.get_leaf_node(0, size))
        self.assertTrue(top_left_leaf.bottom.right is world.get_leaf_node(size, size))

        self.assertTrue(top_left_leaf is not top_left_leaf.right)
        self.assertTrue(top_left_leaf is not top_left_leaf.right.bottom)
        self.assertTrue(top_left_leaf is not top_left_leaf.right.bottom.left)
        self.assertTrue(top_left_leaf is top_left_leaf.right.bottom.left.top)

    def test_get_node_size(self):
        size = 2 ** 5
        min_size = 5
        world = Tree(0, 0, size, None, min_size)
        collider = LeafLinker(world)

        self.assertEqual(world.get_leaf_node(0, 0).size, collider.get_node_size(size, min_size))

    def test_get_coords(self):
        size = 2 ** 2
        min_size = 1
        world = Tree(0, 0, size, None, min_size)
        LeafLinker(world).map_adjacent_nodes()

        top_left_leaf = world.get_leaf_node(0, 0)

        self.assertEqual(top_left_leaf.coord, (0, 0))
        self.assertEqual(top_left_leaf.right.coord, (1, 0))
        self.assertEqual(top_left_leaf.right.right.coord, (2, 0))
        self.assertEqual(top_left_leaf.right.right.right.coord, (3, 0))

        self.assertEqual(top_left_leaf.bottom.coord, (0, 1))
        self.assertEqual(top_left_leaf.bottom.right.coord, (1, 1))
        self.assertEqual(top_left_leaf.bottom.right.right.coord, (2, 1))
        self.assertEqual(top_left_leaf.bottom.right.right.right.coord, (3, 1))

        self.assertEqual(top_left_leaf.bottom.bottom.coord, (0, 2))
        self.assertEqual(top_left_leaf.bottom.bottom.right.coord, (1, 2))
        self.assertEqual(top_left_leaf.bottom.bottom.right.right.coord, (2, 2))
        self.assertEqual(top_left_leaf.bottom.bottom.right.right.right.coord, (3, 2))

        self.assertEqual(top_left_leaf.bottom.bottom.bottom.coord, (0, 3))
        self.assertEqual(top_left_leaf.bottom.bottom.bottom.right.coord, (1, 3))
        self.assertEqual(top_left_leaf.bottom.bottom.bottom.right.right.coord, (2, 3))
        self.assertEqual(top_left_leaf.bottom.bottom.bottom.right.right.right.coord, (3, 3))