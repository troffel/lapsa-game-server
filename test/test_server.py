import socket
import time
from network import cmd
from unittest import TestCase
import Server
import threading


class SimpleClient:

    def __init__(self, port, host=None, network_callback=None):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.port = port
        self.host = host if host is not None else socket.gethostname()
        self.network_callback = network_callback

        self._is_listening = False

    def join(self):
        # get local machine name
        host = self.host
        # host = 'localhost'
        port = self.port

        # connection to hostname on the port.
        self.socket.connect((host, port))

    def answer_handshake(self):
        self.socket.send(cmd.Cmd.TYPE_HANDSHAKE_RESPONSE.encode())

    def start_listen(self):
        self._is_listening = True
        thread = threading.Thread(target=self._listen)
        thread.daemon = True
        thread.start()

    def stop_listening(self):
        self._is_listening = False

    def _listen(self):
        while self._is_listening:
            data = self.socket.recv(1024)
            if data:
                # Decode and remove the trailing \n
                self.network_callback(data.decode().rstrip())
            else:
                print("CLIENT_LOST_CONNECTION")
                self.stop_listening()

    def move(self, x, y):
        print("CLIENT: Moving {},{}".format(x, y))
        self.socket.send((cmd.Cmd.TYPE_MOVE + cmd.Cmd.to_move(x, y)).encode())

    def leave(self):
        self.socket.close()


class TestServer(TestCase):

    def test_join(self):

        def response(msg):
            self.reply = msg
            self.client.stop_listening()
            self.server.stop()

        self.reply = None
        self.server = Server.Server(9000)
        self.client = SimpleClient(9000, network_callback=lambda msg: response(msg))
        self.server.start()
        time.sleep(.5)
        self.client.join()
        self.client.start_listen()

        time.sleep(1)
        self.assertEqual(self.reply, cmd.Cmd.TYPE_HANDSHAKE)

    def test_join_move(self):

        response_list = [cmd.Cmd.TYPE_HANDSHAKE, cmd.Cmd.TYPE_IS_PLAYABLE]

        def response(msg):
            print("CLIENT: received: " + msg)
            if response_list:
                self.assertEqual(msg[:2], response_list.pop(0))
            self.reply = msg

        self.reply = None
        server = Server.Server(9000)
        client = SimpleClient(9000, network_callback=lambda msg: response(msg))
        server.start()
        time.sleep(.5)
        client.join()
        client.start_listen()

        while len(response_list) > 1: # wait for first expected response
            pass

        client.answer_handshake()

        while not response_list: # wait for last expected response
            pass

        time.sleep(4)

        self.assertEqual(self.reply[:2], cmd.Cmd.TYPE_UI_STATE)
        print(self.reply)
        self.assertEqual(self.reply[3:], "2:2:Bob")

        client.move(2, 2)

        time.sleep(3)

        self.assertEqual(self.reply[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply[3:], "4:4:Bob")

        client.move(1, -1)

        time.sleep(3)

        self.assertEqual(self.reply[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply[3:], "5:3:Bob")

        client.stop_listening()
        server.stop()

    def test_2_join_move(self):

        def response(client, msg):

            if client.response_list:
                self.assertEqual(msg[:2], client.response_list.pop(0))

            if client is client_a:
                print("CLIENT_A: received: " + msg)
                self.reply_a = msg
            else:
                print("CLIENT_B: received: " + msg)
                self.reply_b = msg

        self.reply_a = None
        self.reply_b = None
        server = Server.Server(9000)
        client_a = SimpleClient(9000, network_callback=lambda msg: response(client_a, msg))
        client_a.response_list = [cmd.Cmd.TYPE_HANDSHAKE, cmd.Cmd.TYPE_IS_PLAYABLE]
        client_b = SimpleClient(9000, network_callback=lambda msg: response(client_b, msg))
        client_b.response_list = [cmd.Cmd.TYPE_HANDSHAKE, cmd.Cmd.TYPE_IS_PLAYABLE]
        server.start()
        time.sleep(.5)
        client_a.join()
        client_a.start_listen()
        client_b.join()
        client_b.start_listen()

        while len(client_a.response_list) > 1 or len(client_b.response_list) > 1: # wait for first expected response
            pass

        client_a.answer_handshake()
        client_b.answer_handshake()

        while not client_a.response_list or not client_a.response_list: # wait for last expected response
            pass

        time.sleep(4)

        self.assertEqual(self.reply_a[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_b[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_a[3:], "2:2:Bob,2:2:Bob")
        self.assertEqual(self.reply_b[3:], "2:2:Bob,2:2:Bob")

        client_a.move(2, 2)

        time.sleep(3)

        self.assertEqual(self.reply_a[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_b[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_a[3:], "4:4:Bob,2:2:Bob")
        self.assertEqual(self.reply_b[3:], "4:4:Bob,2:2:Bob")

        client_a.move(1, -1)

        time.sleep(3)

        self.assertEqual(self.reply_a[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_b[:2], cmd.Cmd.TYPE_UI_STATE)
        self.assertEqual(self.reply_a[3:], "5:3:Bob,2:2:Bob")
        self.assertEqual(self.reply_b[3:], "5:3:Bob,2:2:Bob")

        client_a.stop_listening()
        client_b.stop_listening()
        server.stop()



