from unittest import TestCase
from models.map import Tree
from models.map import LeafLinker
from geometry import geometry
from structures import blocking
from misc import printer


class TestGeometry(TestCase):

    # create 4x4 tileset
    # check for all diagonal path, that it correctly detects if any block within is blocked

    def test_has_blocking_tile(self):
        size = 2 ** 2
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()

        top_left = tree.get_leaf_node(0, 0)
        top_right = tree.get_leaf_node(size - 1, 0)
        bottom_right = tree.get_leaf_node(size - 1, size - 1)
        bottom_left = tree.get_leaf_node(0, size - 1)
        from_to_sets = [
            (top_left, bottom_right),
            (top_right, bottom_left),
            (bottom_left, top_right),
            (bottom_right, top_left),
        ]

        for point_set in from_to_sets:
            for x in range(4):
                for y in range(4):
                    blocked_tile = tree.get_leaf_node(x,y)

                    self.assertFalse(geometry.Geometry.has_blocking_tile(point_set[0], point_set[1]))

                    blocked_tile.structures.append(blocking.Blocking())
                    self.assertTrue(geometry.Geometry.has_blocking_tile(point_set[0], point_set[1]))

                    for i in range(4):
                        for j in range(4):
                            tree.get_leaf_node(i,j).structures.append(blocking.Blocking())

                    self.assertTrue(geometry.Geometry.has_blocking_tile(point_set[0], point_set[1]))

                    for i in range(4):
                        for j in range(4):
                            tree.get_leaf_node(i, j).structures = [] # clear

    def test_is_between_lines(self):
        point = geometry.euclid.Point2(3, 3)

        direction = geometry.euclid.Vector2(0.89, 1.79)
        line_a = geometry.euclid.Line2(geometry.euclid.Point2(0.66, 3.55), direction)
        line_b = geometry.euclid.Line2(geometry.euclid.Point2(6.45, 0.66), direction)

        self.assertTrue(geometry.Geometry.is_between_lines(point, line_a, line_b))

        self.assertTrue(geometry.Geometry.is_between_lines(point, line_b, line_a))

    def test_is_inside_angled_square(self):

        def rect_45_angle():
            top_left = geometry.euclid.Point2(1, 3)
            top_right = geometry.euclid.Point2(3, 5)
            bottom_left = geometry.euclid.Point2(3, 1)
            bottom_right = geometry.euclid.Point2(5, 3)

            outside = [
                geometry.euclid.Point2(1, 1),
                geometry.euclid.Point2(2, 1),
                geometry.euclid.Point2(4, 1),
                geometry.euclid.Point2(5, 1),
                geometry.euclid.Point2(5, 2),
                geometry.euclid.Point2(5, 4),
                geometry.euclid.Point2(5, 5),
                geometry.euclid.Point2(4, 5),
                geometry.euclid.Point2(2, 5),
                geometry.euclid.Point2(1, 5),
                geometry.euclid.Point2(1, 4),
                geometry.euclid.Point2(1, 2)
            ]

            # Diamond shape, basically all points of 90 degree rect
            inside = [
                geometry.euclid.Point2(3, 1),
                geometry.euclid.Point2(2, 2),
                geometry.euclid.Point2(3, 2),
                geometry.euclid.Point2(4, 2),
                geometry.euclid.Point2(1, 3),
                geometry.euclid.Point2(2, 3),
                geometry.euclid.Point2(3, 3),
                geometry.euclid.Point2(4, 3),
                geometry.euclid.Point2(5, 3),
                geometry.euclid.Point2(2, 4),
                geometry.euclid.Point2(3, 4),
                geometry.euclid.Point2(4, 4),
                geometry.euclid.Point2(3, 5)
            ]

            for point in outside:
                self.assertFalse(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_right, bottom_right),
                        geometry.euclid.Line2(top_left, bottom_left),
                        )
                    ))
                )

            for point in inside:
                self.assertTrue(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_left, bottom_left),
                        geometry.euclid.Line2(top_right, bottom_right),
                        )
                    ))
                )

        def rect_36_angle(): #roughly 36,87 D:
            top_left = geometry.euclid.Point2(1, 2)
            top_right = geometry.euclid.Point2(2, 5)
            bottom_left = geometry.euclid.Point2(4, 1)
            bottom_right = geometry.euclid.Point2(5, 4)

            outside = [
                geometry.euclid.Point2(1, 1),
                geometry.euclid.Point2(2, 1),
                geometry.euclid.Point2(3, 1),
                geometry.euclid.Point2(5, 1),
                geometry.euclid.Point2(5, 2),
                geometry.euclid.Point2(5, 3),
                geometry.euclid.Point2(5, 5),
                geometry.euclid.Point2(4, 5),
                geometry.euclid.Point2(3, 5),
                geometry.euclid.Point2(1, 5),
                geometry.euclid.Point2(1, 4),
                geometry.euclid.Point2(1, 3)
            ]

            inside = [
                geometry.euclid.Point2(4, 1),
                geometry.euclid.Point2(1, 2),
                geometry.euclid.Point2(2, 2),
                geometry.euclid.Point2(3, 2),
                geometry.euclid.Point2(4, 2),
                geometry.euclid.Point2(2, 3),
                geometry.euclid.Point2(3, 3),
                geometry.euclid.Point2(4, 3),
                geometry.euclid.Point2(2, 4),
                geometry.euclid.Point2(3, 4),
                geometry.euclid.Point2(4, 4),
                geometry.euclid.Point2(5, 4),
                geometry.euclid.Point2(2, 5)
            ]

            for point in outside:
                self.assertFalse(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_left, bottom_left),
                        geometry.euclid.Line2(top_right, bottom_right),
                        )
                    ))
                )

            for point in inside:
                self.assertTrue(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_left, bottom_left),
                        geometry.euclid.Line2(top_right, bottom_right),
                        )
                    ))
                )

        def rect_18_angle():  # roughly 18,43 D:
            top_left = geometry.euclid.Point2(1, 4)
            top_right = geometry.euclid.Point2(4, 5)
            bottom_left = geometry.euclid.Point2(2, 1)
            bottom_right = geometry.euclid.Point2(5, 2)

            outside = [
                geometry.euclid.Point2(1, 1),
                geometry.euclid.Point2(3, 1),
                geometry.euclid.Point2(4, 1),
                geometry.euclid.Point2(5, 1),
                geometry.euclid.Point2(1, 2),
                geometry.euclid.Point2(1, 3),
                geometry.euclid.Point2(5, 3),
                geometry.euclid.Point2(5, 4),
                geometry.euclid.Point2(1, 5),
                geometry.euclid.Point2(2, 5),
                geometry.euclid.Point2(5, 5)
            ]

            inside = [
                geometry.euclid.Point2(2, 1),
                geometry.euclid.Point2(2, 2),
                geometry.euclid.Point2(3, 2),
                geometry.euclid.Point2(4, 2),
                geometry.euclid.Point2(5, 2),
                geometry.euclid.Point2(2, 3),
                geometry.euclid.Point2(3, 3),
                geometry.euclid.Point2(4, 3),
                geometry.euclid.Point2(1, 4),
                geometry.euclid.Point2(2, 4),
                geometry.euclid.Point2(3, 4),
                geometry.euclid.Point2(4, 4),
                geometry.euclid.Point2(4, 5)
            ]

            for point in outside:
                self.assertFalse(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_left, bottom_left),
                        geometry.euclid.Line2(top_right, bottom_right),
                        )
                    ))
                )

            for point in inside:
                self.assertTrue(
                    geometry.Geometry.is_inside_angled_square(point, (
                        (
                        geometry.euclid.Line2(top_left, top_right),
                        geometry.euclid.Line2(bottom_left, bottom_right),
                        ), (
                        geometry.euclid.Line2(top_left, bottom_left),
                        geometry.euclid.Line2(top_right, bottom_right),
                        )
                    ))
                )

        rect_45_angle()
        rect_36_angle()
        rect_18_angle()

    def test_get_corners_from_lined_shape(self):

        top = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(1, 0)
        )
        bottom = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 4), geometry.euclid.Vector2(1, 0)
        )
        left = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(0, 1)
        )
        right = geometry.euclid.Line2(
            geometry.euclid.Point2(4, 1), geometry.euclid.Vector2(0, 1)
        )

        corners = geometry.Geometry.get_corners_from_lined_shape(((top, bottom), (left, right)))

        self.assertTrue(geometry.euclid.Point2(1, 1) in corners)
        self.assertTrue(geometry.euclid.Point2(1, 4) in corners)
        self.assertTrue(geometry.euclid.Point2(4, 1) in corners)
        self.assertTrue(geometry.euclid.Point2(4, 4) in corners)

    def test_has_corners_overlaid(self):
        top = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(1, 0)
        )
        bottom = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 4), geometry.euclid.Vector2(1, 0)
        )
        left = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(0, 1)
        )
        right = geometry.euclid.Line2(
            geometry.euclid.Point2(4, 1), geometry.euclid.Vector2(0, 1)
        )

        shape_a = ((top, bottom), (left, right))

        top = geometry.euclid.Line2(
            geometry.euclid.Point2(3, 3), geometry.euclid.Vector2(1, 0)
        )
        bottom = geometry.euclid.Line2(
            geometry.euclid.Point2(3, 6), geometry.euclid.Vector2(1, 0)
        )
        left = geometry.euclid.Line2(
            geometry.euclid.Point2(3, 3), geometry.euclid.Vector2(0, 1)
        )
        right = geometry.euclid.Line2(
            geometry.euclid.Point2(6, 3), geometry.euclid.Vector2(0, 1)
        )

        shape_b = ((top, bottom), (left, right))

        self.assertTrue(geometry.Geometry.has_corners_overlaid(shape_a, shape_b))

        top = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(1, 0)
        )
        bottom = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 4), geometry.euclid.Vector2(1, 0)
        )
        left = geometry.euclid.Line2(
            geometry.euclid.Point2(1, 1), geometry.euclid.Vector2(0, 1)
        )
        right = geometry.euclid.Line2(
            geometry.euclid.Point2(4, 1), geometry.euclid.Vector2(0, 1)
        )

        shape_a = ((top, bottom), (left, right))

        top = geometry.euclid.Line2(
            geometry.euclid.Point2(10, 10), geometry.euclid.Vector2(1, 0)
        )
        bottom = geometry.euclid.Line2(
            geometry.euclid.Point2(10, 13), geometry.euclid.Vector2(1, 0)
        )
        left = geometry.euclid.Line2(
            geometry.euclid.Point2(10, 10), geometry.euclid.Vector2(0, 1)
        )
        right = geometry.euclid.Line2(
            geometry.euclid.Point2(13, 10), geometry.euclid.Vector2(0, 1)
        )

        shape_b = ((top, bottom), (left, right))

        self.assertFalse(geometry.Geometry.has_corners_overlaid(shape_a, shape_b))

    def test_has_blocking_bounding_square(self):
        #TODO: test for blocking square inside path, since no corners then overlap

        size = 2 ** 3
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()
        top_left = tree.get_leaf_node(0, 0)

        top_left.structures.append(blocking.Blocking())

        self.assertTrue(
            geometry.Geometry.has_blocking_angled_bounding_rect((1.9, 1.9), (2.9, 1.9), 1, tree)
        )

        top_left.right.right.right.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.bottom.bottom.structures.append(blocking.Blocking())

        tree.get_leaf_node(2, 4).structures.append(blocking.Blocking())
        tree.get_leaf_node(6, 2).structures.append(blocking.Blocking())

        printer.Printer().print_map(tree)


        self.assertTrue(
            geometry.Geometry.has_blocking_angled_bounding_rect((2, 4), (6, 2), 2, tree)
        )

        self.assertTrue(
            geometry.Geometry.has_blocking_angled_bounding_rect((2, 4), (6, 2), 1, tree)
        )

    def test_has_blocking_bounding_circle(self):
        # TODO: test for blocking square inside path, since no corners then overlap

        size = 2 ** 3
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()
        top_left = tree.get_leaf_node(0, 0)

        top_left.structures.append(blocking.Blocking())

        self.assertFalse(
            geometry.Geometry.has_blocking_angled_bounding_circle((1.9, 1.9), (2.9, 1.9), 1, tree)
        )

        top_left.right.right.right.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.bottom.structures.append(blocking.Blocking())
        top_left.right.right.right.bottom.bottom.bottom.bottom.structures.append(blocking.Blocking())

        tree.get_leaf_node(2, 4).structures.append(blocking.Blocking())
        tree.get_leaf_node(6, 2).structures.append(blocking.Blocking())

        printer.Printer().print_map(tree)

        self.assertTrue(
            geometry.Geometry.has_blocking_angled_bounding_circle((2, 4), (6, 2), 2, tree)
        )

        self.assertTrue(
            geometry.Geometry.has_blocking_angled_bounding_circle((2, 4), (6, 2), 1, tree)
        )

    def test_get_cross_points(self):
        point = geometry.euclid.Point2(2, 2)
        cross_line = geometry.euclid.Line2(point, geometry.euclid.Point2(4, 2))
        point_a, point_b = geometry.Geometry.get_cross_points(point, cross_line, 2)

        self.assertTrue(point_b == (0, 2))
        self.assertTrue(point_a == (4, 2))

    def test_get_furthest_point(self):
        point = (3, 3)

        # opts right off of
        opt_1_a = (5, 4)
        opt_2_a = (5, 3)
        self.assertTrue(geometry.Geometry.get_furthest_point(point, opt_1_a, opt_2_a) == opt_1_a)

        # opts left off of
        opt_1_b = (1, 1)
        opt_2_b = (1, 2)
        self.assertTrue(geometry.Geometry.get_furthest_point(point, opt_1_b, opt_2_b) == opt_1_b)

    def test_get_bounding_points(self):

        p_from = (1, 2)
        p_to = (4, 2)
        radius = 2

        pos = geometry.Geometry.get_bounding_points(p_from, p_to, radius)

        self.assertTrue((-1, 0) in pos[0])
        self.assertTrue((-1, 4) in pos[0])
        self.assertTrue((6, 0) in pos[1])
        self.assertTrue((6, 4) in pos[1])