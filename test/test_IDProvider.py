from unittest import TestCase
from network.idprovider import IDProvider

class TestIDProvider(TestCase):
    def test_get(self):

        provider = IDProvider(10)

        numbers = set()
        for i in range(provider.size - 1):
            numbers.add(provider.get())

        self.assertEqual(len(numbers), provider.size - 1)
        self.assertEqual(provider.get(), 9)
        self.assertTrue(provider.get() is None)

    def test_release(self):
        provider = IDProvider(10)

        try:
            provider.release(0)
        except Exception as e:
            self.assertEqual(str(e), "Attempted to release when no IDs where in use!")


        id = provider.get()

        try:
            provider.release(id + 1) # +1 to simply get something not currently in use
        except Exception as e:
            self.assertEqual(str(e), "Attempted to release {} unused id, number was in free_nums: True".format(id+1))

        provider.release(id)

    def test_get_status(self):

        provider = IDProvider(10)

        self.assertEqual(provider.get_status(), (provider.size, 0))

        for k in range(5):
            provider.get()

        self.assertEqual(provider.get_status(), (5, 5))
        provider.get()
        self.assertEqual(provider.get_status(), (4, 6))
        provider.release(0)
        provider.release(1)
        self.assertEqual(provider.get_status(), (6, 4))

        for i in range(6):
            provider.get()

        self.assertEqual(provider.get_status(), (0, provider.size))




