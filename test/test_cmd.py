from unittest import TestCase
from network import cmd
from models import player


class TestCmd(TestCase):
    def test_to_move(self):

        self.assertEqual(cmd.Cmd.to_move(2,-3), "202103")
        self.assertEqual(cmd.Cmd.to_move(2, 2), "202202")
        self.assertEqual(cmd.Cmd.to_move(15, 4), "215204")
        self.assertEqual(cmd.Cmd.to_move(-15, 4), "115204")

    def test_from_move(self):
        self.assertEqual(cmd.Cmd.from_move("202103"), (2, -3))
        self.assertEqual(cmd.Cmd.from_move("202202"), (2, 2))
        self.assertEqual(cmd.Cmd.from_move("215204"), (15, 4))
        self.assertEqual(cmd.Cmd.from_move("115204"), (-15, 4))

    def test_to_players(self):
        # connection isn't optional normally, but we don't need it for this test
        players = [
            player.Player(0, None),
            player.Player(1, None),
            player.Player(2, None),
            player.Player(3, None)
        ]
        self.assertEqual(cmd.Cmd.to_players(players[0]), "0:0:Bob")
        self.assertEqual(cmd.Cmd.to_players(*players), "0:0:Bob,0:0:Bob,0:0:Bob,0:0:Bob")

    def test_is_movement(self):
        action = cmd.Cmd.TYPE_MOVE + "123456"

        self.assertTrue(cmd.Movement.is_movement(action))

        self.assertFalse(cmd.Movement.is_movement("12345678"))

    def test_to_ui_state(self):
        players = [
            player.Player(0, None),
            player.Player(1, None)
        ]
        self.assertEqual(cmd.Cmd.to_ui_state(cmd.Cmd.to_players(*players)), "11-0:0:Bob,0:0:Bob")
        self.assertNotEqual(cmd.Cmd.to_ui_state(cmd.Cmd.to_players(*players)), "fubar")
