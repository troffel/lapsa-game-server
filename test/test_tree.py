from unittest import TestCase
from models.map import Tree
from models.map import LeafLinker
from structures import blocking


class TestTree(TestCase):

    def test_get_leaf_node(self):
        # make 4x4
        size = 2 ** 2
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()

        leaf = tree.get_leaf_node(0,0)

        self.assertTrue(leaf is not tree.get_leaf_node(0, leaf.size))
        self.assertTrue(leaf is not tree.get_leaf_node(leaf.size, 0))
        self.assertTrue(leaf is not tree.get_leaf_node(leaf.size, leaf.size))
        self.assertTrue(leaf is leaf.right.bottom.left.top)

    def test_make_children(self):
        size = 2 ** 2
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        self.assertIsNotNone(tree.children[0].children[0])
        self.assertFalse(tree.children[0].children[0].children)

    def test_is_blocking(self):
        size = 1
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)

        self.assertFalse(tree.is_blocking())
        tree.structures.append(blocking.Blocking())
        self.assertTrue(tree.is_blocking())

    def test_has(self):
        size = 2 ** 2
        min_size = 2
        tree = Tree(0, 0, size, None, min_size)

        self.assertTrue(tree.has(0, 0))
        self.assertTrue(tree.get_leaf_node(0, 0).has(0, 0))
        self.assertTrue(tree.get_leaf_node(min_size, min_size).has(size-1, size-1))

    def test_is_valid_move(self):
        size = 2 ** 4
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()

        # start top left
        start = tree.get_leaf_node(0, 0)

        # check block-checks work with inheritance
        class Dummy(blocking.Blocking):
            pass

        # add blocking
        start.right.structures.append(Dummy())
        start.bottom.structures.append(Dummy())

        # try walking past them
        self.assertFalse(tree.is_valid_move((1, 1), (2, 2), 1))
        self.assertFalse(tree.is_valid_move((0, 0), (2, 0), 1))
        self.assertFalse(tree.is_valid_move((0, 0), (0, 2), 1))

        start.bottom.structures.clear()

        # TODO: Make these tests more expansive

    def test_get_bounding_tiles(self):
        size = 2 ** 3
        min_size = 1
        tree = Tree(0, 0, size, None, min_size)
        LeafLinker(tree).map_adjacent_nodes()

        points = [(1, 1), (2, 4), (5, 3), (4, 0)]

        b_tiles = tree.get_bounding_tiles(points)

        self.assertTrue(tree.get_leaf_node(1, 0).coord == b_tiles[0].coord)
        self.assertTrue(tree.get_leaf_node(5, 4).coord == b_tiles[1].coord)