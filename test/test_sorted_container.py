from unittest import TestCase
from misc import container


class TestSortedContainer(TestCase):
    class TestClass:
        def __init__(self, x, y, name):
            self.x = x
            self.y = y
            self.name = name

        def move(self, x_delta, y_delta):
            self.x += x_delta
            self.y += y_delta

    def test_changing_attributes(self):
        items = [
            TestSortedContainer.TestClass(0, 0, "1"),
            TestSortedContainer.TestClass(1, 1, "2"),
            TestSortedContainer.TestClass(2, 2, "3"),
            TestSortedContainer.TestClass(3, 3, "4"),
            TestSortedContainer.TestClass(4, 4, "5"),
            TestSortedContainer.TestClass(5, 5, "6")
        ]

        sc = container.SortedContainer(items, None, None, "x", "y")

        sc.sort()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 3)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] in nearby)
        self.assertTrue(items[4] in nearby)

        items[3].move(4, 4)

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 3)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] in nearby)
        self.assertTrue(items[4] in nearby)

        sc.sort()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 2)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] not in nearby)
        self.assertTrue(items[4] in nearby)

        items[3].move(-4, -4)

        sc.sort()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 3)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] in nearby)
        self.assertTrue(items[4] in nearby)

    def test_get_nearby_stacked(self):
        items = [
            TestSortedContainer.TestClass(0, 0, "1"),
            TestSortedContainer.TestClass(1, 1, "2"),
            TestSortedContainer.TestClass(2, 2, "3"),
            TestSortedContainer.TestClass(2, 2, "4"),
            TestSortedContainer.TestClass(2, 2, "5"),
            TestSortedContainer.TestClass(3, 3, "6")
        ]

        sc = container.SortedContainer(items, None, None, "x", "y")

        sc.sort()


        nearby = sc.get_nearby((2, "x", 1), (2, "y", 1))
        self.assertEqual(len(nearby), 5)

        self.assertTrue(items[1] in nearby)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] in nearby)
        self.assertTrue(items[4] in nearby)
        self.assertTrue(items[5] in nearby)

        nearby = sc.get_nearby((2, "x", 20000), (2, "y", 20000))
        self.assertEqual(len(nearby), 6)

    def test_get_nearby(self):

        items = [
            TestSortedContainer.TestClass(0, 0, "1"),
            TestSortedContainer.TestClass(1, 1, "2"),
            TestSortedContainer.TestClass(2, 2, "3"),
            TestSortedContainer.TestClass(3, 3, "4"),
            TestSortedContainer.TestClass(4, 4, "5"),
            TestSortedContainer.TestClass(5, 5, "6")
        ]

        sc = container.SortedContainer(items, None, None, "x", "y")

        sc.sort()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(nearby)
        self.assertTrue(len(nearby) == 3)
        self.assertTrue(items[2] in nearby)
        self.assertTrue(items[3] in nearby)
        self.assertTrue(items[4] in nearby)

        extra = TestSortedContainer.TestClass(4, 4, "7")
        sc.add(extra)

        sc.handle_queued()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 4)
        self.assertTrue(extra in nearby)

        sc.remove(extra)

        sc.handle_queued()

        nearby = sc.get_nearby((3, "x", 1), (3, "y", 1))

        self.assertTrue(len(nearby) == 3)
        self.assertTrue(extra not in nearby)
