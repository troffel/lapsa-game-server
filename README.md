# README #

### What is this repository for? ###

A POC aimed at making a larger game server, using TCP.
Only supports x,y coordinates and runs with some artificial pausing in the code.

Project aimed to try to only deliver exactly the information the client needs, and nothing more. E.g. ensuring that clients only know about other clients within certain coordinates, etc.

Written in python3, set up in Pycharm.