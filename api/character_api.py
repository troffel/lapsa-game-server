from models import player


class CharacterApi:

    @staticmethod
    def get_player_data(callback):
        """

        :param callback: takes a player.Data as parameter
        :return:
        """

        callback(player.Data(0))