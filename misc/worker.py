import time
import threading

class Worker:

    def __init__(self):
        self._workables = []
        self._running = False
        self._worker = None
        self._work_list_lock = threading.Lock()

    def register(self, workable):
        """

        :param workable: An object that exposes an on_work() and a is_busy() method
        :return:
        """

        assert hasattr(workable, "on_work") and hasattr(workable, "is_busy")
        with self._work_list_lock:
            self._workables.append(workable)

    def unregister(self, workable):
        self._workables.remove(workable)

    def start(self):

        if self._running or self._worker is not None and self._worker.isAlive:
            return

        self._running = True
        self._worker = threading.Thread(target=self._do_work)
        self._worker.daemon = True
        self._worker.start()

    def stop(self):
        self._running = False

    def _do_work(self):
        """
        thread task
        :return:
        """

        while self._running:
            with self._work_list_lock:
                for task in self._workables:
                    if self._running: # extra check so stopping will be more timely
                        if not task.is_busy():
                            task.on_work()
            time.sleep(0)  # python yield

        print("Worker stopped")