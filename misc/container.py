from threading import Lock
import bisect
import math


class SortedContainer:
    """
    TODO: Main list, and a sorted list for each attribute we sort by.
    """

    def __init__(self, items, on_added = None, on_removed = None, *attribute_names):
        """

        :param items:
        :param attributes: Attribute should be a number
        :param on_added(item)
        :param on_removed(item)
        """
        self.lock = Lock()

        self._lock_appending_remove = Lock()
        self._pending_remove = []
        self._pending_add = []

        self._on_added = on_added
        self._on_removed = on_removed

        self._is_sorting = False

        # key(attr), val(lock, list)
        self._orders = {}

        for attr in attribute_names:
            sortable_list = []
            sortable_list.extend(items)
            self._orders[attr] = (Lock(), sortable_list)

    # The x-value of player gets manipulated externally, and we resort every now and then.
    def add(self, item):
        with self._lock_appending_remove:
            self._pending_add.append(item)

    def remove(self, item):
        # if a player is removed, we don't remove it from the x,y lists since that automatically should happen on next
        # sort
        with self._lock_appending_remove:
            self._pending_remove.append(item)

    def handle_queued(self):
        """
        empty any queues into lists
        :return:
        """
        if self._has_queued():
            with self.lock:
                with self._lock_appending_remove:
                    for item in self._pending_remove:
                        for attr, data in self._orders.items():
                            self._orders[attr][1].remove(item)
                        self._pending_remove.remove(item)
                        self._on_removed(item) if self._on_removed is not None else None

                    for item in self._pending_add:
                        for attr, data in self._orders.items():
                            self._orders[attr][1].append(item)
                        self._pending_add.remove(item)
                        self._on_added(item) if self._on_added is not None else None
                self.sort()

    def _has_queued(self):
        return self._pending_add or self._pending_remove

    def sort(self):
        # Sorting is a mutating operation, so we lock during this operation
        # sorts, maintaining only locks on the currently sorting list
        #sort
        if self._is_sorting:
            return False

        self._is_sorting = True
        if self._has_queued():
            self.handle_queued()
        else:
            for attr, data in self._orders.items():
                with data[0]: #use the lock
                    data[1].sort(key=lambda x: getattr(x, attr))
        self._is_sorting = False

    def _index_of_item(self, items, attr_val, attr, inclusive_direction):
        """

        :param items:
        :param item:
        :param attr:
        :param inclusive_direction: 0 for downwards, 1 for upwards
        :return:
        """
        assert inclusive_direction == 1 or inclusive_direction == 0

        start, end = 0, (len(items) - 1)
        while start <= end:
            mid = (start + end) // 2
            if attr_val == getattr(items[mid], attr):

                if inclusive_direction == 0:
                    p = mid
                    while p - 1 >= 0 and items[p - 1] == attr_val:
                        p -= 1
                    return p

                else:
                    p = mid
                    while p + 1 < len(items) and items[p + 1] == attr_val:
                        p += 1
                    return p

            if attr_val < getattr(items[mid], attr):
                end = mid - 1
            else:  # elem > arr[mid]
                start = mid + 1

        return 0 if inclusive_direction == 0 else len(items) - 1

    def _get_nearby(self, attr, val, radius):
        left = self._index_of_item(self._orders[attr][1], val - radius, attr, 0)
        right = self._index_of_item(self._orders[attr][1], val + radius, attr, 1)
        nearby = self._orders[attr][1][left:right + 1]
        return nearby

    def get_nearby(self, *val_attr_sets):
        """

        :param val_attr_sets: [(val, attr, radius),]
        :return:
        """

        nearbys = set()

        for attr_set in val_attr_sets:
            nearby = set(self._get_nearby(attr_set[1], attr_set[0], attr_set[2]))

            if not nearbys:
                # First loop, add all
                nearbys.update(nearby)
            else:
                # Only keep values that are nearby on all the queried attributes
                nearbys.intersection(nearby)

        return list(nearbys)