
class Printer:

    def print_map(self, tree_map):
        top_left = tree_map.get_leaf_node(0, 0)

        v_tile = top_left
        while v_tile is not None:

            h_tile = v_tile
            map_string = ""
            while h_tile is not None:
                map_string += ".." if h_tile.is_blocking() else "[]"
                h_tile = h_tile.right

            print(map_string)

            v_tile = v_tile.bottom