from misc import container
from threading import Lock
from models import player as _player
import time


class State:

    _STATE_UPDATE_INTERVAL_SEC = 3
    _STATE_UPDATE_NEARBY_INTERVAL_SEC = 1
    _STATE_NEARBY_RADIUS = 200

    def __init__(self):

        # TODO: Really should make it more transparent by using an object rather than a tuple
        self.players_lock = Lock()
        self.players = {} # val is (player, lock) lock used when accessing player subs
        self.sorted_container = container.SortedContainer(
            self.players,
            lambda player: player.connection.on_ready(),
            lambda player: player.connection.on_removed(),
            "x", "y"
        )
        self.last_update = time.time()
        self.last_nearby_update = time.time()

        self._is_updating_nearby = False
        self._is_sorting_players = False

    def on_player_join(self, connection):
        """
        On join, we load the player
        :param connection:
        :return:
        """

        def on_player_ready(player):
            with self.players_lock:
                if player.connection.session_id in self.players:
                    raise Exception("Can't add a session ID that is already added")
                self.players[player.connection.session_id] = (player, Lock())
            self.sorted_container.add(player)

        _player.Player.load(connection, lambda player: on_player_ready(player))

    def on_player_leave(self, connection):
        #player = self.players[connection.session_id]
        with self.players_lock:
            del self.players[connection.session_id]
        self.sorted_container.remove(connection.player)

    def _update_near_players(self):
        self._is_updating_nearby = True
        with self.players_lock:
            for session_id, player in self.players.items():
                # Set subs!
                with player[1]:
                    player[0].subs.clear()
                    player[0].subs.extend(
                        self.sorted_container.get_nearby(
                            (player[0].x, "x", self._STATE_NEARBY_RADIUS),
                            (player[0].y, "y", self._STATE_NEARBY_RADIUS),
                        )
                    )
        self._is_updating_nearby = False


    def _sort_players_if_needed(self):
        self._is_sorting_players = True
        curr_time = time.time()
        if self.last_update + State._STATE_UPDATE_INTERVAL_SEC < curr_time:
            did_sort = self.sorted_container.sort()
            if did_sort:
                self.last_update = curr_time
        self._is_sorting_players = False

    def on_work(self):
        # update nearby if subscriptions if needed
        curr_time = time.time()
        if self.last_nearby_update + State._STATE_UPDATE_NEARBY_INTERVAL_SEC < curr_time:
            self._update_near_players()
            self.last_nearby_update = curr_time

        self._sort_players_if_needed()

    def is_busy(self):
        return self._is_sorting_players or self._is_updating_nearby