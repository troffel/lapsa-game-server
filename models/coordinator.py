import queue
import threading

class MoveCoordinator:
    """
    Coordinator handles all gameworld movement
    """

    def __init__(self, map_):
        self._map = map_
        self._queue = queue.Queue(1000)

        self._worker = threading.Thread(target=self._work_queue)
        self._worker.daemon = True

        self._running = False

    def _add_queue(self, reject_func, func, param):
        try:
            self._queue.put_nowait((reject_func, func, param))
            return True
        except queue.Full as e:
            print("Coordinator reached full queue, bad congestion happening")
            raise e

    def _work_queue(self):
        """
        Run in thread!
        :return:
        """
        if self._running:
            return
        else:
            self._running = True

        while self._running:
            try:
                # Give it a timeout so we can actually shut it down timely
                reject_func, func, param = self._queue.get(timeout=5)
            except queue.Empty as e:
                continue

            if self._map.is_valid_move(*param):
                func()
            else:
                reject_func()

        print("Coordinator stopped")

    def stop(self):
        self._running = False

    def start(self):
        self._worker.start()

    def move(self, player, from_pos, to_pos, radius):
        """
        Attempts to move
        :param from_pos: (x, y)
        :param to_pos: (x, y)
        :param radius: >0
        :return: False if queue is full
        """
        def move():
            player.x = to_pos[0]
            player.y = to_pos[1]

        def reject():
            player.connection.rejected()

        self._add_queue(reject, move, (from_pos, to_pos, radius))

        # WE GOT HERE WITH A MOVE, BUT IT IS NOT REFLECTED IN THE STATE THE CLIENT IS GETTING
        # ADD_QUEUE PROB ISNT WORKING :o)