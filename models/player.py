from api import character_api


class Player:

    @staticmethod
    def load(connection, on_loaded_callback):

        def on_callback(data):
            player = Player(data, connection)
            connection.player = player
            on_loaded_callback(player)

        # Eventually will be async
        character_api.CharacterApi.get_player_data(lambda data: on_callback(data))

    def __init__(self, data, connection):
        """

        :param data: stored player data
        :param connection:
        """
        self.connection = connection

        self.subs = []

        self.id = data.id
        self.x = data.x
        self.y = data.y
        self.radius = data.radius
        self.name = data.name

    """ We are fine with object comparison, and since this would also require __hash__ to be overwritten
    def __eq__(self, other):
        return isinstance(other, Player) and other.connection.session_id == self.connection.session_id
    """


class Data:

    def __init__(self, id):
        self.id = id
        # Due to how even rounded path have outlinings calculated as rects, getting too close to side is bad
        # sqrt(radius^2 + radius^2) > x or y, will be invalid, as the corner can angle outside map when moving
        # diagonally.
        self.x = 2
        self.y = 2
        self.radius = 1
        self.name = "Bob"
