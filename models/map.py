from structures import blocking
from models import coordinator
from geometry import geometry

class TreeMembers:
    # Enums
    TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT = range(4)


class Tree:
    """
    Tree structure for optimising collision lookups for structures
    """

    def __init__(self, x, y, size, parent, min_size):
        """

        :param x:
        :param y:
        :param size: Should be an even number to produce event splits
        :param parent: None if top
        :param min_size While each child is x/2 by y/2, we don't want the size to become less than min_size. A recursive bound.
        """
        assert min_size > 0

        # List of stuctures on this map node. Order from 'ground and up', meaning [dirt, grass, fence, cloud]
        self.structures = []

        self.x = x
        self.y = y
        self.size = size
        self.parent = parent

        # Only exists on leaf nodes
        self.top = None
        self.right = None
        self.bottom = None
        self.left = None

        # Only exist on leaf nodes
        # XY Tuple
        self.coord = None

        self.coordinator = coordinator.MoveCoordinator(self)

        self.min_size = min_size

        # [top_left, top_right, bottom_left, bottom_right]
        self.children = []

        if size / 2 >= self.min_size:
            self.make_children()

    def get_coordinator(self):
        return self.coordinator

    def is_blocking(self):
        for structure in self.structures:
            if isinstance(structure, blocking.Blocking):
                return True
        return False

    def get_leaf_node(self, x, y):
        for child in self.children:
            if child.has(x,y):
                if child.children:
                    return child.get_leaf_node(x, y)
                else:
                    return child
        return None

    def __str__(self):
        return "x: {}, y: {}, size: {}".format(self.x, self.y, self.size)

    def make_children(self):
        x = self.x
        y = self.y
        size = self.size
        new_size = size / 2

        # top left
        self.children.append(Tree(x, y, new_size, self, self.min_size))
        # top right
        self.children.append(Tree(x + new_size, y, new_size, self, self.min_size))
        # bottom left
        self.children.append(Tree(x, y + new_size, new_size, self, self.min_size))
        # bottom right
        self.children.append(Tree(x + new_size, y + new_size, new_size, self, self.min_size))

    def has(self, x, y):
        """
        Determins if this node covers a certain coordinate
        :param x:
        :param y:
        :return:
        """
        return self.x <= x and \
               self.y <= y and \
               self.x + self.size > x and \
               self.y + self.size > y

    def is_valid_move(self, cord_from, cord_to, radius):
        """
        While we could do with just the move_to coord, since we can look up the player,
        it's easier to just also parse that along.
        :param cord_from: (x,y)
        :param cord_to: (x,y)
        :param radius:
        :return: True if the targeted position isn't blocked. And the both between the two positions isn't blocked either.
        """
        return not geometry.Geometry.has_blocking_angled_bounding_circle(cord_from, cord_to, radius, self)

    def get_bounding_tiles(self, points):
        """

        :param points: [(x, y),..]
        :return: lower bounding (x, y), higher bounding (x, y)
        """
        highest_x = None
        lowest_x = None
        highest_y = None
        lowest_y = None
        for point in points:
            if highest_x is None or point[0] > highest_x:
                highest_x = point[0]

            if lowest_x is None or point[0] < lowest_x:
                lowest_x = point[0]

            if highest_y is None or point[1] > highest_y:
                highest_y = point[1]

            if lowest_y is None or point[1] < lowest_y:
                lowest_y = point[1]

        return self.get_leaf_node(lowest_x, lowest_y), self.get_leaf_node(highest_x, highest_y)


class LeafLinker:
    """
    Links together adjacent leafs in the map quad-tree
    """
    def __init__(self, tree, ):
        self.__tree = tree

    def map_adjacent_nodes(self):
        node_size = self.get_node_size(self.__tree.size, self.__tree.min_size)

        offset = node_size / 2

        #print("offset: {}, nodesize: {}".format(offset, node_size))

        x, y = 0, 0
        cur_width = offset
        cur_height = offset
        while cur_height < self.__tree.size:
            while cur_width < self.__tree.size:

                leaf = self.__tree.get_leaf_node(cur_width, cur_height)

                leaf.coord = (x, y)

                #print("Node: ({},{})".format(cur_width, cur_height))

                # check above isn't out of bound
                if cur_height - node_size > 0:
                    leaf.top = self.__tree.get_leaf_node(cur_width, cur_height - node_size)

                # check right isn't out of bound
                if cur_width + node_size < self.__tree.size:
                    leaf.right = self.__tree.get_leaf_node(cur_width + node_size, cur_height)

                # check bottom isn't out of bound
                if cur_height + node_size < self.__tree.size:
                    leaf.bottom = self.__tree.get_leaf_node(cur_width, cur_height + node_size)

                # check left isn't out of bound
                if cur_width - node_size > 0:
                    leaf.left = self.__tree.get_leaf_node(cur_width - node_size, cur_height)

                cur_width += node_size
                x += 1
            x = 0
            y += 1
            cur_height += node_size
            cur_width = offset

    def get_node_size(self, size, min_size):
        size = self.__tree.size
        min_size = self.__tree.min_size

        while size / 2 >= min_size:
            size /= 2

        return size

