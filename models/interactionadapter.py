from network import cmd
import time
import threading


class InteractionAdapter:

    _STATE_BROADCAST_INTERVAL_SEC = 1

    def __init__(self, state, coordinator):
        self.game_state = state
        self.game_coordinator = coordinator

        self._running = False
        self._broadcaster = None

    def on_action_in(self, connection, command):
        """

        :param connection:
        :param cmd: string
        :return:
        """
        if cmd.Movement.is_movement(command):
            player = connection.player
            # to_x and to_y define movement delta
            delta_x, delta_y = cmd.Cmd.from_move(command[2:])
            to_x = player.x + delta_x
            to_y = player.y + delta_y

            self.game_coordinator.move(player, (player.x, player.y), (to_x, to_y), player.radius)

    def start(self):

        if self._running or self._broadcaster is not None and self._broadcaster.isAlive:
            return

        self._running = True
        self._broadcaster = threading.Thread(target=self._broadcast)
        self._broadcaster.daemon = True
        self._broadcaster.start()

    def stop(self):
        self._broadcaster._running = False

    def _broadcast(self):
        """
        This should be threaded
        :return:
        """
        while self._running:
            self._push_screen_data_to_players()
            time.sleep(InteractionAdapter._STATE_BROADCAST_INTERVAL_SEC)

        print("Interaction Adapter stopped")

    def _push_screen_data_to_players(self):
        for session_id, player in self.game_state.players.items():
            # Translate entity locations to locations relative to user viewport
            with player[1]: # lock
                if player[0].connection.is_in_game():
                    players_string = cmd.Cmd.to_players(*player[0].subs)
                    player[0].connection.send(cmd.Cmd.to_ui_state(players_string))
