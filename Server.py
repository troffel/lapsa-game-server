from models import map
from models import state
from models import interactionadapter
from models import coordinator
from misc import worker
from network import adapter
import socket


class Server:

    def __init__(self, port, host=None):
        self.port = port
        self.host = host if host is not None else socket.gethostname()
        self.map = self._build_map()
        self.state = state.State()

        self.worker = worker.Worker()
        self.worker.register(self.state)

        self.coordinator = coordinator.MoveCoordinator(self.map)

        self.interaction_adapter = interactionadapter.InteractionAdapter(self.state, self.coordinator)

        self.listener = adapter.Adapter.Listener(
            lambda connection: self.state.on_player_join(connection),
            lambda connection: self.state.on_player_leave(connection),
            lambda connection, interaction_string: self.interaction_adapter.on_action_in(connection, interaction_string)
        )

        self.adapter = adapter.Adapter(self.host, self.port, 100, self.listener)

        # Figure out how we broadcast the results of the above? List of pending broadcasts with intended receivers?
        #      network adapter will handle all network traffic through one thread
        #      all 'heavy' work will quickly be diverted to datastructures and then handled on the common-task thread.

        # Figure out how the client avoids knowing the size of the map, e.g. not being able to tell how far they
        # are from the edge
        #               Put map cord 0,0 in center of map?
        #               Let client send along it cords and server the tells it where entities are based on that?
        #               Server receives movements from client, returns everything needed to build screen
        #
        #               Client sends movement commands, server responds with position of everything on screen

        # Screen state for a player should provide extra information to the specific player and leave out specifics of
        # others, until needed

    def _build_map(self):
        game_map = map.Tree(0, 0, 2**5, None, 5)
        map.LeafLinker(game_map).map_adjacent_nodes()
        return game_map

    def start(self):
        self.worker.start()
        self.interaction_adapter.start()
        self.adapter.start()
        self.coordinator.start()

        print("SERVER: started")

    def stop(self):
        self.worker.stop()
        self.interaction_adapter.stop()
        self.adapter.stop()
        self.coordinator.stop()


