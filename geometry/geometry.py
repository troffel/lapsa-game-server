from geometry import euclid


class Geometry:
    @staticmethod
    def is_blocking_bounding_circle():
        """NOT IMPLEMENTED"""
        # TODO: This
        pass

    @staticmethod
    def has_blocking_bounding_square(p_from, p_to, radius, map):
        """

        :param p_from: (x, y)
        :param p_to: (x, y)
        :param radius:
        :param map:
        :return:
        """

        # Directional vector

        dir_vec = (p_from[0] - p_to[0], p_from[1] - p_to[1])

        # Bottom right
        if dir_vec[0] > 0 and dir_vec[1] > 0:
            pass

        # Bottom
        if dir_vec[0] == 0 and dir_vec[1] > 0:
            pass

        # Bottom left
        if dir_vec[0] < 0 and dir_vec[1] > 0:
            pass

        # Left
        if dir_vec[0] < 0 and dir_vec[1] == 0:
            pass

        # Top left
        if dir_vec[0] < 0 and dir_vec[1] < 0:
            pass

        # Top
        if dir_vec[0] == 0 and dir_vec[1] < 0:
            pass

        # Top right
        if dir_vec[0] > 0 and dir_vec[1] < 0:
            pass

        # Right
        if dir_vec[0] > 0 and dir_vec[1] == 0:
            pass

        # Check if blocking in bounding

        # Check if inside 2 formed triangles

        pass

    @staticmethod
    def is_in_top_triangle():
        pass

    @staticmethod
    def is_in_bot_triangle():
        pass

    @staticmethod
    def has_blocking_angled_bounding_rect(p_from, p_to, radius, map):
        """

        :param p_from: (x, y)
        :param p_to: (x, y)
        :param radius:
        :param map:
        :return:
        """

        assert radius > 0

        bounding_ends = Geometry.get_bounding_points(p_from, p_to, radius)
        bounding_points = [*bounding_ends[0], *bounding_ends[1]]

        bounding_left = euclid.Line2(euclid.Point2(*bounding_ends[0][0]), euclid.Point2(*bounding_ends[0][1]))
        bounding_right = euclid.Line2(euclid.Point2(*bounding_ends[1][0]), euclid.Point2(*bounding_ends[1][1]))
        # 'Hat' the endling line vektor to get the 90 degree direction
        hat = euclid.Vector2(bounding_left.v[1] * -1, bounding_left.v[0])
        bounding_top = euclid.Line2(euclid.Point2(*bounding_ends[0][0]), hat)
        bounding_bottom = euclid.Line2(euclid.Point2(*bounding_ends[0][1]), hat)

        bounding_tiles = map.get_bounding_tiles(bounding_points)

        # If point is None, we're out of bounds of map, definitely not allowed!
        if bounding_tiles[0] is None or bounding_tiles[1] is None:
            return True

        blocking_tiles = Geometry.has_blocking_tile(bounding_tiles[0], bounding_tiles[1])
        if not blocking_tiles:
            return False

        # TODO: Check if blocking square is inside path

        for tile in blocking_tiles:

            top_left = euclid.Point2(tile.x, tile.y)
            top_right = euclid.Point2(tile.x + tile.size, tile.y)
            bottom_left = euclid.Point2(tile.x, tile.y + tile.size)
            bottom_right = euclid.Point2(tile.x + tile.size, tile.y + tile.size)

            til_top = euclid.Line2(top_left, top_right)
            til_bottom = euclid.Line2(bottom_left, bottom_right)
            til_left = euclid.Line2(top_left, bottom_left)
            til_right = euclid.Line2(top_right, bottom_right)

            blocking = Geometry.has_corners_overlaid(
                [(til_top, til_bottom), (til_left, til_right)],
                [(bounding_top, bounding_bottom), (bounding_left, bounding_right)]
            )

            if blocking:
                return blocking

        return False

    @staticmethod
    def has_corners_overlaid(shape_a, shape_b):
        """

        :param shape_a: 2 tuples of euclid.Line2()
        :param shape_b: 2 tuples of euclid.Line2()
        :return:
        """
        # check corners of blocking tiles inside path
        # check if path corners inside blocking tiles

        corners = Geometry.get_corners_from_lined_shape(shape_a)

        for corner in corners:
            if Geometry.is_inside_angled_square(corner, shape_b):
                return True

        corners = Geometry.get_corners_from_lined_shape(shape_b)
        for corner in corners:
            if Geometry.is_inside_angled_square(corner, shape_a):
                return True

        return False

    @staticmethod
    def get_corners_from_lined_shape(shape):
        """
        No guarantee about order of line sets in shape
        :param shape: ((left, right), (top, bottom))
        :return: 4 corner points
        """
        points = [
            shape[0][0].intersect(shape[1][0]),  # top left
            shape[0][0].intersect(shape[1][1]),  # bottom left
            shape[0][1].intersect(shape[1][1]),  # bottom right
            shape[0][1].intersect(shape[1][0])  # top right
        ]

        assert len(points) == 4
        return points

    @staticmethod
    def get_bounding_points(p_from, p_to, radius):
        """
        Calculates the bounding points when including radius
        TODO: Avoid object creations
        :param p_from: (x, y)
        :param p_to: (x, y)
        :param radius:
        :param rounded
        :return: bounding points in the form of (x, y) tuple
        """

        euclid_from = euclid.Point2(*p_from)
        euclid_to = euclid.Point2(*p_to)

        line = euclid.Line2(euclid_from, euclid_to)

        # create reversed line by calculating the y from the reversed angle of the original line

        from_points = Geometry.get_cross_points(euclid_from, euclid.Line2(euclid_from, euclid_to), radius)
        from_point = euclid.Point2(*Geometry.get_furthest_point(p_to, *from_points))

        start_cross_line = euclid.Line2(
            from_point,
            euclid.Vector2(line.v.y, line.v.x * -1)  # inverse vector
        )

        from_crosspoints = Geometry.get_cross_points(euclid.Point2(*from_point), start_cross_line, radius)

        to_points = Geometry.get_cross_points(euclid_to, euclid.Line2(euclid_from, euclid_to), radius)
        to_point = euclid.Point2(*Geometry.get_furthest_point(p_from, *to_points))

        end_cross_line = euclid.Line2(
            to_point,
            euclid.Vector2(line.v.y, line.v.x * -1)  # inverse vector
        )

        to_crosspoints = Geometry.get_cross_points(to_point, end_cross_line, radius)

        bounding_points = [from_crosspoints, to_crosspoints]

        return bounding_points

    @staticmethod
    def get_furthest_point(point, opt_1, opt_2):
        """

        :param point: (x, y)
        :param opt_1: (x, y)
        :param opt_2: (x, y)
        :return:
        """
        opt_1_dist = abs(point[0] - opt_1[0]) + abs(point[1] - opt_1[1])
        opt_2_dist = abs(point[0] - opt_2[0]) + abs(point[1] - opt_2[1])

        return opt_1 if opt_1_dist > opt_2_dist else opt_2

    @staticmethod
    def get_cross_points(point, cross_line, radius):
        """
        from infinite line, gets points that intersect with provided line at given radius
        :param point: euclid.Point2()
        :param cross_line: euclid.Line2()
        :param radius:
        :return: points (x, y)
        """
        start_circle = euclid.Circle(point, float(radius))
        inside_segment = start_circle.intersect(cross_line)

        point_a = (inside_segment.p.x, inside_segment.p.y)
        point_b = (point_a[0] + inside_segment.v.x, point_a[1] + inside_segment.v.y)

        return point_a, point_b

    @staticmethod
    def has_blocking_tile(from_tile, to_tile):
        """

        :param from_tile: map tile
        :param to_tile: map tile
        :return:
        """
        # Returns list of blocking tiles, 0 if none. Note: empty list = boolean false expression
        tile_diff_x, tile_diff_y = to_tile.coord[0] - from_tile.coord[0], to_tile.coord[1] - from_tile.coord[1]

        scan_from_y, scan_to_y = scan_from_x, scan_to_x = from_tile, to_tile

        blocking_tiles = []

        while scan_from_y is not None:
            while scan_from_x is not None:
                if scan_from_x.is_blocking():
                    blocking_tiles.append(scan_from_x)
                scan_from_x = scan_from_x.right if tile_diff_x > 0 else scan_from_x.left

            scan_from_y = scan_from_y.bottom if tile_diff_y > 0 else scan_from_y.top
            # reset after loop
            scan_from_x = scan_from_y
        return blocking_tiles

    @staticmethod
    def is_point_inside_square(point, lines):
        """

        :param point: point to check for euclide.Point2()
        :param lines: 2 tuples of euclid.Line2() ((top, bottom),(left, right))
        :return: True if inside
        """
        return Geometry.is_inside_angled_square(point, lines)

    @staticmethod
    def is_inside_angled_square(point, lines):
        """
        Bit more expensive than non_angled, but could potentially work for all shapes
        :param lines ((top, bottom), (left, right))
        :param point euclid.point2()
        :return:
        """
        between_set_1 = Geometry.is_between_lines(point, *lines[0])
        between_set_2 = Geometry.is_between_lines(point, *lines[1])
        return between_set_1 and between_set_2

    @staticmethod
    def int_round(val):
        # one decimal is enough, but lets just take 3 to make it more accurate
        return float("{0:.5f}".format(val))

    @staticmethod
    def is_between_lines(point, line_a, line_b):
        """

        :param point: euclid.Point2()
        :param line_a: euclid.Line2()
        :param line_b: euclid.Line2()
        :return:
        """

        def dist(line_a, line_b):
            # line_diff = line_a.distance(line_b) seemed unreliable at times, so doing this instead

            vector = line_a.v
            hat_v = euclid.Vector2(-1 * vector[1], vector[0])

            diagonal = euclid.Line2(line_a.p, hat_v)

            a = diagonal.intersect(line_a)
            b = diagonal.intersect(line_b)
            dist = a.distance(b)

            return dist

        line_diff = dist(line_a, line_b)
        p_dist_a = point.distance(line_a)
        p_dist_b = point.distance(line_b)
        r_line_diff = Geometry.int_round(line_diff)
        r_dist_diff = Geometry.int_round(p_dist_a + p_dist_b)
        return r_line_diff == r_dist_diff

    @staticmethod
    def get_parallel_sets(lines):
        """
        NOTE: MUTATES PARSED LIST
        :param lines: euclid.Line2() lines, expects 4 lines
        :return:
        """
        assert len(lines) == 4

        line = None
        for i, comp_line in enumerate(lines):
            if line is None:
                line = comp_line
                continue

            # no intersections = parallel
            if line.intersect(comp_line) is None:
                lineSets = [(line, comp_line)]
                # other set must be remaining two lines

                other_set = list(lines)
                other_set.pop(i)
                other_set.pop(0)

                assert other_set[0].v == other_set[1].v

                lineSets.append((other_set[0], other_set[1]))

                return lineSets
        return None

    @staticmethod
    def hat(vector):
        return euclid.Vector2(-1 * vector[1], vector[0])

    @staticmethod
    def has_blocking_angled_bounding_circle(p_from, p_to, radius, map):
        """

        :param p_from: (x, y)
        :param p_to: (x, y)
        :param radius:
        :param map:
        :return:
        """

        assert radius > 0

        bounding_ends = Geometry.get_bounding_points(p_from, p_to, radius)
        bounding_points = [*bounding_ends[0], *bounding_ends[1]]

        bounding_tiles = map.get_bounding_tiles(bounding_points)

        # If point is None, we're out of bounds of map, definitely not allowed!
        if bounding_tiles[0] is None or bounding_tiles[1] is None:
            return True

        blocking_tiles = Geometry.has_blocking_tile(bounding_tiles[0], bounding_tiles[1])
        if not blocking_tiles:
            return False

        # TODO: Check if blocking square is inside path

        geo_path = GeoPath(p_from, p_to, radius)

        for tile in blocking_tiles:

            top_left = euclid.Point2(tile.x, tile.y)
            top_right = euclid.Point2(tile.x + tile.size, tile.y)
            bottom_left = euclid.Point2(tile.x, tile.y + tile.size)
            bottom_right = euclid.Point2(tile.x + tile.size, tile.y + tile.size)

            til_top = euclid.LineSegment2(top_left, top_right)
            til_bottom = euclid.LineSegment2(bottom_left, bottom_right)
            til_left = euclid.LineSegment2(top_left, bottom_left)
            til_right = euclid.LineSegment2(top_right, bottom_right)

            in_top = geo_path.is_inside(til_top)
            in_bot = geo_path.is_inside(til_bottom)
            in_left = geo_path.is_inside(til_left)
            in_right = geo_path.is_inside(til_right)

            if in_top or in_bot or in_left or in_right:
                return True

        return False


class GeoPath:

    def __init__(self, p_from, p_to, radius, circular=True):
        """

        :param p_from: (x,y)
        :param p_to:  (x,y)
        :param radius: int
        :param circular:
        """
        self.p_from = p_from if isinstance(p_from, euclid.Point2) else euclid.Point2(*p_from)
        self.p_to = p_to if isinstance(p_to, euclid.Point2) else euclid.Point2(*p_to)
        self.c_from = euclid.Circle(self.p_from, float(radius))
        self.c_to = euclid.Circle(self.p_to, float(radius))
        self.circular = circular

    def is_inside(self, linesegment):
        """
        
        :param linesegment: 
        :return: 
        """

        to_int = linesegment.intersect(self.c_to)
        if to_int:
            return True

        from_int = linesegment.intersect(self.c_from)
        if from_int:
            return True

        dir_vec = euclid.Vector2(self.p_to.x - self.p_from.x, self.p_to.y - self.p_from.y)

        revers_dir_line = Geometry.hat(dir_vec)

        line_seg_a = self.c_from.intersect(euclid.Line2(self.p_from, revers_dir_line))

        line_seg_b = self.c_to.intersect(euclid.Line2(self.p_to, revers_dir_line))

        top = euclid.Line2(line_seg_a.p1, line_seg_b.p1)
        bottom = euclid.Line2(line_seg_a.p2, line_seg_b.p2)
        left = euclid.Line2(line_seg_a.p1, line_seg_a.p2)
        right = euclid.Line2(line_seg_b.p1, line_seg_b.p2)

        b = linesegment.intersect(bottom)
        b = Geometry.is_between_lines(b, left, right) if b else b
        t = linesegment.intersect(top)
        t = Geometry.is_between_lines(t, left, right) if t else t
        l = linesegment.intersect(left)
        l = Geometry.is_between_lines(l, top, bottom) if l else l
        r = linesegment.intersect(right)
        r = Geometry.is_between_lines(r, top, bottom) if r else r
        p1 = Geometry.is_point_inside_square(linesegment.p1, (
            (top, bottom),
            (left, right),
        ))
        p2 = Geometry.is_point_inside_square(linesegment.p2, (
            (top, bottom),
            (left, right),
        ))
        return b or t or l or r or p1 or p2
